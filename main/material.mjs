//// Xx.

export default class Material extends THREE.MeshPhysicalMaterial {

    addNoise (options) {
        const { swapXY, size, opacity, consts } = options
        const { ctx, w, h, lStart, dStart, lDiff, dDiff, prng } = consts
        let lite, dark, y, x, r, g, b, dia, xPos, yPos

        for (y=0; y<h; y+=size) {
            lite = (lDiff * y / h) + lStart
            dark = (dDiff * y / h) + dStart
            for (x=0; x<w; x+=size) {
                r = ~~( prng.nextFloat() * (lite-dark) + dark )
                dia = prng.nextFloat() * size
                xPos = swapXY ? y+dia/2 : x+dia/2
                yPos = swapXY ? x+dia/2 : y+dia/2
                ctx.beginPath()
                ctx.fillStyle = `rgba(${r},${r},${r},${opacity})`
                if (2 < size) {
                    ctx.arc(xPos, yPos, dia, 0, Math.PI*2)
                    ctx.fill()
                } else { // just use a little square for small dots
                    ctx.fillRect(xPos, yPos, size, size)
                }
            }
        }

    }

    constructor (options) {
        super()

        const
            $el = this.$el = document.querySelector(options.selector)
          , ctx = this.ctx = this.$el.getContext('2d')
          , w = $el.width
          , h = $el.height
          , lStart = options.lite.start
          , dStart = options.dark.start
          , lDiff = options.lite.end - lStart
          , dDiff = options.dark.end - dStart
          , prng = new Random(options.color)
          , consts = { ctx, w, h, lStart, dStart, lDiff, dDiff, prng }

        this.addNoise({ swapXY:false, size:16, opacity:1,   consts })
        this.addNoise({ swapXY:false, size: 8, opacity:0.8, consts })
        this.addNoise({ swapXY:true,  size: 4, opacity:0.6, consts })
        this.addNoise({ swapXY:true,  size: 2, opacity:0.5, consts })
        this.addNoise({ swapXY:false, size: 1, opacity:0.4, consts })

        // ctx.fillStyle = `rgb(0,0,255)`
        // ctx.fillRect(0, 4, w, 4)
        // ctx.fillStyle = `rgb(255,0,0)`
        // for (let y=10; y<h; y+=10)
        //     ctx.fillRect(0, y, w, 1)
        // ctx.fillStyle = `rgb(0,255,0)`
        // ctx.fillRect(4, 0, 4, h)
        // ctx.fillStyle = `rgb(255,255,0)`
        // ctx.fillRect(w-8, 0, 4, h)
        // ctx.fillStyle = `rgb(255,128,0)`
        // for (let x=10; x<w; x+=10)
        //     ctx.fillRect(x, 0, 1, h)
        ctx.fillStyle = `rgb(0,0,0)`
        ctx.fillRect(0, ~~(h*0.85), w, ~~(h*0.16)) // marks area never visible


        const texture = new THREE.CanvasTexture($el)
        this.side = THREE.DoubleSide
        this.map = texture
        this.bumpMap = texture
        // this.color = new THREE.Color(options.color)
        // this.opacity = 0.1
        // this.transparent = true
        // this.flatShading = true
        // this.reflectivity = 1
        this.metalness = 0.1 // default is 0.5
        this.bumpScale = 1
    }

    get texture () { return this._texture }

}



//// From https://gist.github.com/blixt/f17b47c62508be59987b

/**
 * Creates a pseudo-random value generator. The seed must be an integer.
 *
 * Uses an optimized version of the Park-Miller PRNG.
 * http://www.firstpr.com.au/dsp/rand31/
 */
function Random(seed) {
  this._seed = seed % 2147483647;
  if (this._seed <= 0) this._seed += 2147483646;
}

/**
 * Returns a pseudo-random value between 1 and 2^32 - 2.
 */
Random.prototype.next = function () {
  return this._seed = this._seed * 16807 % 2147483647;
};


/**
 * Returns a pseudo-random floating point number in range [0, 1).
 */
Random.prototype.nextFloat = function (opt_minOrMax, opt_max) {
  // We know that result of next() will be 1 to 2147483646 (inclusive).
  return (this.next() - 1) / 2147483646;
};
