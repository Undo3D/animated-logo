//// A control panel for the scene’s lights.

import Panel from './panel.mjs'
import state from '../state.mjs'

export default class PanelLights extends Panel {

    constructor (options) {
        super(options)

        this.scene = options.scene

        state.panel.lights = { }

        const isAside = !! document.querySelector('aside ' + this.selector)




        //// SINGLE LIGHT

        this.addSlider({
            id: 'singlehue'
          , panelState: state.panel.lights
          , isAside
          , min: 0
          , max: 360
          , labelRounder: v => ~~v
          , labelUnit: '°'
          , onSetValue: v => {
                state.singlehue = ~~v
                this.updateSingle()
            }
        })

        this.addSlider({
            id: 'singlelightness'
          , panelState: state.panel.lights
          , isAside
          , min: 0
          , max: 100
          , labelRounder: v => ~~v
          , labelUnit: '%'
          , onSetValue: v => {
                state.singlelightness = ~~v
                this.updateSingle()
            }
        })

        this.addLightswitch({
            id: 'singlelightswitch'
          , onClick: evt => {
                state.singleshow = ! state.singleshow
                this.updateSingle()
            }
        })




        const $space1 = document.createElement('div')
        $space1.style.height = '3px'
        this.$el.appendChild($space1)




        //// PAIR OF LIGHTS

        this.addSlider({
            id: 'pairhue'
          , panelState: state.panel.lights
          , isAside
          , min: 0
          , max: 360
          , labelRounder: v => ~~v
          , labelUnit: '°'
          , onSetValue: v => {
                state.pairhue = ~~v
                this.updatePair()
            }
        })

        this.addSlider({
            id: 'pairlightness'
          , panelState: state.panel.lights
          , isAside
          , min: 0
          , max: 100
          , labelRounder: v => ~~v
          , labelUnit: '%'
          , onSetValue: v => {
                state.pairlightness = ~~v
                this.updatePair()
            }
        })

        this.addLightswitch({
            id: 'pairlightswitch'
          , isPair: true
          , onClick: evt => {
                state.pairshow = ! state.pairshow
                this.updatePair()
            }
        })

    }

    updateSingle () {
        const
            hsl = `hsl(${state.singlehue}, 100%, ${state.singlelightness}%)`
        this.scene.pointLightSingle.color = new THREE.Color(hsl)
        this.scene.lightBulbSingleMaterial.color = new THREE.Color(hsl)
        if (this.$singlelightswitch)
            this.$singlelightswitch.$lighticon.style.backgroundColor = hsl
        if (this.$singlelightswitch)
            if (state.singleshow)
                this.$singlelightswitch.$el.classList.add('active')
            else
                this.$singlelightswitch.$el.classList.remove('active')
    }

    updatePair () {
        const
            pairhue1 = (state.pairhue-60+360)%360
          , pairhue2 = (state.pairhue+60)%360
          , hsl1 = `hsl(${pairhue1}, 100%, ${state.pairlightness}%)`
          , hsl2 = `hsl(${pairhue2}, 100%, ${state.pairlightness}%)`
        this.scene.pointLightPair1.color = new THREE.Color(hsl1)
        this.scene.lightBulbPair1Material.color = new THREE.Color(hsl1)
        this.scene.pointLightPair2.color = new THREE.Color(hsl2)
        this.scene.lightBulbPair2Material.color = new THREE.Color(hsl2)
        if (this.$pairlightswitch) {
            this.$pairlightswitch.$lighticon1.style.backgroundColor = hsl1
            this.$pairlightswitch.$lighticon2.style.backgroundColor = hsl2
        }
        if (this.$pairlightswitch)
            if (state.pairshow)
                this.$pairlightswitch.$el.classList.add('active')
            else
                this.$pairlightswitch.$el.classList.remove('active')
    }

}
