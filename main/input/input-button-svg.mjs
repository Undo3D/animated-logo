//// Image Button.
import InputButton from './input-button.mjs'

export default class InputButtonSvg extends InputButton {
    constructor (options) {
        super(options)
        const s = options.size
        this.$el.title = options.title
        this.$el.innerHTML = `
        <svg x="0px" y="0px" width="${s}px" height="${s}px" viewBox="0 0 ${s} ${s}">
        ${options.svg}
        </svg>`
    }

    get className () { return 'btn svg' }
}
