//// Input.

export default class Input {
    constructor (options) {
        this.$body = document.querySelector('body')

        this.$el = document.createElement(this.tagName)
        this.$el.className = this.className
        this.$el.id = options.id

        if (this.css) {
            const $stylesInHead = document.querySelectorAll('head style')
            $stylesInHead[$stylesInHead.length-1].innerHTML += this.css
        }
    }

    get tagName () { return 'input' }
    get className () { return '' }
    get css () { return null }
}
