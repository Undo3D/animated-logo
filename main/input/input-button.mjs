//// Button.
import Input from './input.mjs'

export default class InputButton extends Input {
    constructor (options) {
        super(options)
        this.$el.addEventListener('click', options.onClick)
    }

    get tagName () { return 'button' }
    get className () { return 'btn' }
}
