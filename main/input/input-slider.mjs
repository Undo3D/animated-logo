//// Slider.
import Input from './input.mjs'
import state from '../state.mjs'

export default class InputSlider extends Input {
    constructor (options) {

        //// The Input class does most of the work setting up the slider wrap.
        super(options)

        this.isAside = options.isAside
        this.min = options.min
        this.max = options.max
        this.labelRounder = options.labelRounder
        this.labelUnit = options.labelUnit
        this.onPreciseBegin = options.onPreciseBegin

        //// eg state.panel.timeline.playtimeState.
        const that = this
        this.thumbState = options.panelState[options.id+'State'] = {
            startClientX: false
          , mostRecentClientX: 0
          , startTime: null
          , prevClickEndTime: -9999 // helps detect a double-click
          , startMarginLeft: 0
          , isEnteringPrecise: false
          , oldInnerHTML: '0'
          , _value: 0

            //// Allow custom action when slider value changes.
          , get value () { return this._value }
          , set value (v) {
                this._value = Math.max( that.min, Math.min(that.max, v) )
                that.updateThumbFromValue()
                options.onSetValue(this._value)
            }
        }

        //// Set up the Slider’s ‘thumb’ (a.k.a. ‘handle’).
        const $thumb = this.$thumb = document.createElement('div')
        $thumb.id = this.$el.id + '-thumb' // eg 'scrub-thumb'
        $thumb.className = 'btn loading' // 'loading' is removed when state is ready
        $thumb.style.marginLeft = 0
        this.$el.appendChild($thumb)

        const on = ($target, evtNames, method) => {
            for ( const evtName of evtNames.split(' ') )
                $target.addEventListener(evtName, method.bind(this) )
        }

        //// Make the thumb jump to a clicked point on the slider.
        on(this.$el, 'mousedown touchstart', this.jumpTo)

        //// Let the user drag the slider.
        on($thumb, 'mousedown touchstart', this.dragBegin)
        on(window, 'mouseup touchend touchcancel', this.dragEnd)
        on(window, 'mousemove touchmove', this.dragMove)

        // on($thumb, 'dblclick', this.preciseBegin)
        on($thumb, 'keydown', this.preciseKeydown)
        on($thumb, 'blur', this.preciseBlur)

        //// `state` should contain an initial value for this Slider, either
        //// from config.defaults or localStorage. @TODO or from anywhere else?
        //// Set the local copy of the value, and update the thumb.
        const initialValue = state[this.$el.id]
        this.thumbState.value = initialValue // will call onSetValue()
        this.updateThumbFromValue()
    }

    get tagName () { return 'div' }
    get className () { return 'slider' }

    get width () {
        if (this.isAside)
            return state.asideWidth - 64
        return state.mainWidth - 128
    }

    get css () {
        const { id } = this.$el
        return `
        body.dragging-${id} #${id}-thumb, #${id}-thumb:hover { background:#3f3; }
        body.dragging-${id} #${id}-thumb { cursor:grabbing; }
    `}


    jumpTo (evt) {
        const { thumbState } = this

        //// Do nothing if the user is currently entering a precise value, or is
        //// currently dragging the thumb.
        if (thumbState.isEnteringPrecise || false !== thumbState.startClientX)
            return evt.preventDefault() // would deselect highlighted text, by default

        //// Update this slider’s value in `state`. onSetValue() will be called.
        this.updateValueFromEvt(evt)
    }

    dragBegin (evt) {
        const { thumbState } = this
        evt.stopPropagation() // otherwise jumpTo() would be called

        //// Do nothing if the user is currently entering a precise value, or is
        //// already dragging the thumb.
        if (thumbState.isEnteringPrecise || false !== thumbState.startClientX)
            return

        //@TODO ignore multitouch

        ////
        const clientX = evt.touches
          ? evt.touches[0].clientX // touchscreen
          : evt.clientX // mouse
        thumbState.startClientX = clientX

        thumbState.startTime = +(new Date())

        ////
        this.$body.classList.add('dragging-'+this.$el.id)
        thumbState.startMarginLeft = parseInt(this.$thumb.style.marginLeft, 10)

        //// Update this slider’s value in `state`. onSetValue() will be called.
        this.updateValueFromEvt(evt)
    }

    dragEnd (evt) {
        const { thumbState } = this

        //// Do nothing if the user is currently entering a precise value, or
        //// was not dragging the thumb.
        if (thumbState.isEnteringPrecise || false === thumbState.startClientX)
            return

        this.$body.classList.remove('dragging-'+this.$el.id)

        //@TODO ignore multitouch

        ////
        const clientX = evt.touches
          ? thumbState.mostRecentClientX // touchscreen
          : evt.clientX // mouse

        //// Deal with clicks and double-clicks, not a drag.
        const now = +(new Date())
        const wasClick = (
            now - thumbState.startTime < 200 // less than 200ms
         && Math.abs(clientX - thumbState.startClientX) < 8 ) // less than 8 pixels (horizontally)
        if (wasClick) {
            if (now - thumbState.prevClickEndTime > 200) { // a single
                thumbState.prevClickEndTime = now
            } else { // a double
                thumbState.prevClickEndTime = -9999
                this.preciseBegin()
            }
        }

        ////
        thumbState.startClientX = false

        //// Update this slider’s value in `state`. onSetValue() will be called.
        // this.updateValueFromEvt(evt)
    }

    dragMove (evt) {
        const { thumbState } = this

        //// Do nothing if the user is currently entering a precise value, or
        //// was not dragging the thumb.
        if (thumbState.isEnteringPrecise || false === thumbState.startClientX)
            return

        //// Ignore multitouch.
        if (evt.touches && 1 !== evt.touches.length) return

        //// Update this slider’s value in `state`. onSetValue() will be called.
        this.updateValueFromEvt(evt)
    }

    preciseBegin () {
        const { thumbState } = this
        if (this.onPreciseBegin) this.onPreciseBegin()
        thumbState.isEnteringPrecise = true
        thumbState.oldInnerHTML = this.$thumb.innerHTML
        this.$thumb.contentEditable = 'true'
        this.selectAll()
    }

    preciseKeydown (evt) {
        const { thumbState } = this

        //// Do nothing if the user is not entering a precise value, or is
        //// dragging the thumb. Also, ignore most keystrokes.
        ////@TODO match everything except digits, copy/paste, delete etc
        if (! thumbState.isEnteringPrecise || false !== thumbState.startClientX
         || /^[a-z]$/i.test(evt.key) )
            return evt.preventDefault()

        //// End entering a value when the Enter or Return key is pressed.
        if ('Enter' === evt.key) {
            evt.preventDefault()
            this.$thumb.blur() // will call preciseBlur()
            return
        }

        //// Deal with nondigit characters.
        if (! /^[-\d]*$/.test(this.$thumb.innerHTML) ) {
            this.$thumb.innerHTML = thumbState.oldInnerHTML
            this.selectAll()
        }
        thumbState.oldInnerHTML = this.$thumb.innerHTML
    }

    preciseBlur (evt) {
        const { thumbState } = this

        //// Do nothing if the user is not entering a precise value, or is
        //// dragging the thumb.
        if (! thumbState.isEnteringPrecise || false !== thumbState.startClientX)
            return

        thumbState.isEnteringPrecise = false
        this.$thumb.contentEditable = 'false'
        this.$thumb.blur()
        thumbState.value = parseInt(this.$thumb.innerHTML, 10)
        this.updateThumbFromValue()
        return
    }




    //// UTILITY

    //// Change the Slider value from a mouse/touch event (clamp if necessary).
    updateValueFromEvt (evt) {
        const { thumbState, width } = this
          , { left } = this.$el.getBoundingClientRect()
          , clientX = (evt.touches ? evt.touches[0] : evt).clientX
          , localX = clientX - left
          , unclamped = localX / width * (this.max-this.min) + this.min
        thumbState.value = unclamped
        thumbState.mostRecentClientX = clientX
        this.updateThumbFromValue()
    }

    //// Move the thumb to the proper position (assumes value is clamped).
    updateThumbFromValue () {
        const { $thumb, thumbState, labelRounder, labelUnit, min, max, width } = this
        $thumb.style.marginLeft =
            (thumbState.value - min) * (width-70) / (max-min) + 'px'
        if (! thumbState.isEnteringPrecise)
            $thumb.innerHTML = labelRounder(thumbState.value) + labelUnit
    }

    selectAll () {
        const range = document.createRange()
        range.selectNodeContents(this.$thumb)
        const selection = window.getSelection()
        selection.removeAllRanges()
        selection.addRange(range)
    }
}
