//// Boots the app.

//// Dependencies.
import './console.mjs'
import config from './config.mjs'
import state from './state.mjs'
import Router from './router.mjs'
import Svg from './svg.mjs'
import Scene from './scene.mjs'
import Material from './material.mjs'
import PanelLights from './panel/panel-lights.mjs'
import PanelPosition from './panel/panel-position.mjs'
import PanelTimeline from './panel/panel-timeline.mjs'
import ListSnapshots from './list/list-snapshots.mjs'
import InputSlider from './input/input-slider.mjs'


////
class Boot {

    constructor () {
        console.log('Loading:\n  ' + config.path);

        //// Remove the preload message.
        const $preload = document.querySelector('#preload')
        $preload.parentNode.removeChild($preload)

        //// We’ve got this far, so this is probably a modern enough browser.
        const $notsupported = document.querySelector('#notsupported')
        $notsupported.parentNode.removeChild($notsupported)

        this.$html = document.querySelector('html')
        this.$body = document.querySelector('body')

        //// Init the router.
        this.router = new Router(this.onWindowResize.bind(this))

        //// Deal with a click on the homepage’s transparent background.
        document.querySelector('#about').addEventListener('click', evt => {
            if ('about' !== evt.target.id) return // clicked on the panel
            evt.preventDefault()
            this.router.onRouteChange('preview')
            history.pushState(null, null, './?/preview')
        })

        //// Generate textures.
        const materials = {
            '#666666': new Material({ selector:'#texture-666666', color:0x666666
              , lite:{ start: 70, end:100 }, dark:{ start: 30, end: 70} })
          , '#808080': new Material({ selector:'#texture-808080', color:0x808080
              , lite:{ start: 80, end:130 }, dark:{ start:  0, end: 80 } })
          , '#999999': new Material({ selector:'#texture-999999', color:0x999999
              , lite:{ start:130, end:180 }, dark:{ start: 80, end:130 } })
          , '#CCCCCC': new Material({ selector:'#texture-CCCCCC', color:0xCCCCCC
              , lite:{ start:180, end:230 }, dark:{ start:130, end:180 }})
          , '#EDEDED': new Material({ selector:'#texture-EDEDED', color:0xEDEDED
              , lite:{ start:255, end:200 }, dark:{ start:200, end:150 } })
        }

        //// Init control panels.
        this.panels = {
            timeline: new PanelTimeline({
                selector: '.panel.timeline'
            })
        }

        //// Init lists.
        this.lists = { }

        //// Load the SVG file and parse points-data.
        this.svg = new Svg({ materials })
        this.svg
           .fetch(config.path)
           .then( this.createScene.bind(this) )
           .then( () => this.lists.snapshots = new ListSnapshots({
                selector:'.list.snapshots'
              , timeline: this.panels.timeline
              , scene: this.scene
            }) )
           .then( () => this.panels.position = new PanelPosition({
                selector:'.panel.position'
              , scene: this.scene
            }) )
           .then( () => this.panels.lights = new PanelLights({
                selector:'.panel.lights'
              , scene: this.scene
            }) )
           .then( () => {
                for (const panelid in this.panels)
                    for (const propertyid in this.panels[panelid])
                        if (this.panels[panelid][propertyid] instanceof InputSlider)
                            this.panels[panelid][propertyid].thumbState.value += 0 // trigger onSetValue()

           })
           .catch( e => console.error(e) )

        //// Set element widths and heights to fit the viewport.
        window.addEventListener( 'resize', this.onWindowResize.bind(this) )
        window.addEventListener( 'orientationchange', this.onWindowResize.bind(this) )
        setTimeout( this.onWindowResize.bind(this), 200 ) // fix Edge bug
        this.onWindowResize()

        //// Show version.
        fetch( new Request('./package.json') )
           .then( response => {
                if (! response.ok) throw Error(response.statusText)
                response.text().then( j => console.log(JSON.parse(j).version) )
            })
           .catch( e => console.error(e) )

        ////
        window.addEventListener( 'hashchange', () => {
            const hashMatch = location.hash.match(
                /^#(playing|paused),(\d+),(\d+),(\d+),(\d+),(show|hide),(\d+),(\d+),(show|hide)$/
            )
            if (null == hashMatch) return
            state.isPlaying       = 'playing' === hashMatch[1] ? true : false
            state.playtime        = +hashMatch[2]
            state.duration        = +hashMatch[3]
            state.singlehue       = +hashMatch[4]
            state.singlelightness = +hashMatch[5]
            state.singleshow      = 'show' === hashMatch[6] ? true : false
            state.pairhue         = +hashMatch[7]
            state.pairlightness   = +hashMatch[8]
            state.pairshow        = 'show' === hashMatch[9] ? true : false
        })

    }

    onWindowResize (evt) {
        const { right:width } = document.documentElement.getBoundingClientRect()

        //// Update the layout.
        if ( this.$html.classList.contains('route-about') // ‘about’...
          || this.$html.classList.contains('route-timeline') // ...or ‘timeline’...
          || this.$html.classList.contains('route-preview') ) { // ...or ‘preview’ route
            state.mainWidth  = Math.min(config.minMainWidth, width - 10 - 10)
            state.asideWidth = 0
        } else if (10 + config.minMainWidth + 10 + config.minAsideWidth + 10 > width) { // single column
            state.mainWidth  = width - 10 - 10
            state.asideWidth = width - 10 - 10
        } else { // double column
            state.mainWidth  = config.minMainWidth
            state.asideWidth = width - config.minMainWidth - 10 - 10 - 10
        }

        //// Tell each Slider to update its thumb position.
        for (const panelid in this.panels)
            for (const propertyid in this.panels[panelid])
                if (this.panels[panelid][propertyid] instanceof InputSlider)
                    this.panels[panelid][propertyid].updateThumbFromValue()

        //// Tell THREE.js that the canvas has changed size.
        if (this.scene) { // available after assets have loaded
            this.scene.renderer.setSize(state.mainWidth, state.mainWidth)
            this.scene.composer.setSize(
                state.mainWidth * config.pixelRatio
              , state.mainWidth * config.pixelRatio
            )
        }

    }

    createScene () {
        this.scene = new Scene(this)
        console.log('Found ' + this.svg.triangles.length + ' tri and ' + this.svg.rects.length + ' rec')
    }

}

try { new Boot() } catch (e) { console.error(e) }
