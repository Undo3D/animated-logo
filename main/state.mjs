//// State.

import config from './config.mjs'


//// Define in-memory cache of HTML elements, and also some localStorage helpers.
const
    cache = {}
  , maybeCache = s => !! (cache[s] = cache[s] || document.querySelector(s) || 0)
  , maybeCacheAll = s => !! (cache[s] = cache[s] || document.querySelectorAll(s) || 0)
  , getItem = localStorage.getItem.bind(localStorage)
  , setItem = localStorage.setItem.bind(localStorage)

//// Do things when modified.
let _mainWidth  = config.defaults.mainWidth
  , _asideWidth = config.defaults.asideWidth

//// Declare variables which may be loaded from localStorage, or the URL hash,
//// or config.defaults.
let _isPlaying
  , _playtime
  , _duration
  , _singlehue
  , _singlelightness
  , _singleshow
  , _pairhue
  , _pairlightness
  , _pairshow
  , _xposition
  , _yposition
  , _zposition

//// Try to read state from the URL’s hash fragment.
const hashMatch = location.hash.match(
    /^#(playing|paused),(\d+),(\d+),(\d+),(\d+),(show|hide),(\d+),(\d+),(show|hide),(-?\d+),(-?\d+),(-?\d+)$/
)
if (null == hashMatch) {
    _isPlaying = getItem('isPlaying')
      ? JSON.parse( getItem('isPlaying') ) : config.defaults.isPlaying
    _playtime = getItem('playtime')
      ? JSON.parse( getItem('playtime') ) : config.defaults.playtime
    _duration = getItem('duration')
      ? JSON.parse( getItem('duration') ) : config.defaults.duration
    _singlehue = getItem('singlehue')
      ? JSON.parse( getItem('singlehue') ) : config.defaults.singlehue
    _singlelightness = getItem('singlelightness')
      ? JSON.parse( getItem('singlelightness') ) : config.defaults.singlelightness
    _singleshow = getItem('singleshow')
      ? JSON.parse( getItem('singleshow') ) : config.defaults.singleshow
    _pairhue = getItem('pairhue')
      ? JSON.parse( getItem('pairhue') ) : config.defaults.pairhue
    _pairlightness = getItem('pairlightness')
      ? JSON.parse( getItem('pairlightness') ) : config.defaults.pairlightness
    _pairshow = getItem('pairshow')
      ? JSON.parse( getItem('pairshow') ) : config.defaults.pairshow
    _xposition = getItem('xposition')
      ? JSON.parse( getItem('xposition') ) : config.defaults.xposition
    _yposition = getItem('yposition')
      ? JSON.parse( getItem('yposition') ) : config.defaults.yposition
    _zposition = getItem('zposition')
      ? JSON.parse( getItem('zposition') ) : config.defaults.zposition
    //@TODO deal with invalid JSON, which makes the app unloadable!
} else {
    _isPlaying       = 'playing' === hashMatch[1] ? true : false
    _playtime        = +hashMatch[2]
    _duration        = +hashMatch[3]
    _singlehue       = +hashMatch[4]
    _singlelightness = +hashMatch[5]
    _singleshow      = 'show' === hashMatch[6] ? true : false
    _pairhue         = +hashMatch[7]
    _pairlightness   = +hashMatch[8]
    _pairshow        = 'show' === hashMatch[9] ? true : false
    _xposition       = +hashMatch[10]
    _yposition       = +hashMatch[11]
    _zposition       = +hashMatch[12]
}


//// The state object.
export default {
    panel: {}
  , list: {}

    //// Do things when modified.
  , get mainWidth () { return _mainWidth }
  , set mainWidth (v) {
        if ( maybeCache('main') )
            cache['main'].style.width = (v+10) + 'px'
        if ( maybeCache('#render') )
            cache['#render'].style.width = cache['#render'].style.height = v + 'px'
        if ( maybeCacheAll('main .panel, main .list, nav.prev, nav.next') )
            for (const $el of cache['main .panel, main .list, nav.prev, nav.next'])
                $el.style.width = v + 'px'
        if ( maybeCache('.btn.svg#play') )
            cache['.btn.svg#play'].style.marginLeft = (v-119) + 'px'
        if ( maybeCache('.btn.svg#pause') )
            cache['.btn.svg#pause'].style.marginLeft = (v-119) + 'px'
        _mainWidth = v
        return v
    }
  , get asideWidth () { return _asideWidth }
  , set asideWidth (v) {
        if ( maybeCache('pre') ) cache['pre'].style.width = v + 'px'
        if ( maybeCacheAll('aside .panel, aside .list') )
            for (const $panelOrList of cache['aside .panel, aside .list'])
                $panelOrList.style.width = v + 'px'
        _asideWidth = v
        return v
    }
  , get isPlaying () {
        document.body.classList[_isPlaying ? 'add' : 'remove']('is-playing')
        return _isPlaying
    }
  , set isPlaying (v) {
        setItem('isPlaying', v)
        document.body.classList[v ? 'add' : 'remove']('is-playing')
        return _isPlaying = v
    }


    //// Persisted.
  , get playtime () { return _playtime }
  , set playtime (v) { setItem('playtime', v); return _playtime = v }
  , get duration () { return _duration }
  , set duration (v) {
        v = Math.max( config.durationMin, Math.min(config.durationMax, v) )
        setItem('duration', v)
        return _duration = v
    }

  , get singlehue () { return _singlehue }
  , set singlehue (v) { setItem('singlehue', v); return _singlehue = v }
  , get singlelightness () { return _singlelightness }
  , set singlelightness (v) { setItem('singlelightness', v); return _singlelightness = v }
  , get singleshow () { return _singleshow }
  , set singleshow (v) { setItem('singleshow', v); return _singleshow = v }

  , get pairhue () { return _pairhue }
  , set pairhue (v) { setItem('pairhue', v); return _pairhue = v }
  , get pairlightness () { return _pairlightness }
  , set pairlightness (v) { setItem('pairlightness', v); return _pairlightness = v }
  , get pairshow () { return _pairshow }
  , set pairshow (v) { setItem('pairshow', v); return _pairshow = v }

  , get xposition () { return _xposition }
  , set xposition (v) { setItem('xposition', v); return _xposition = v }
  , get yposition () { return _yposition }
  , set yposition (v) { setItem('yposition', v); return _yposition = v }
  , get zposition () { return _zposition }
  , set zposition (v) { setItem('zposition', v); return _zposition = v }
}
