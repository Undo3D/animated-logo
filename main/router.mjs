//// Xx.

export default class Router {
    constructor (onWindowResize) {
        this.onWindowResize = onWindowResize

        const buttons = document.querySelectorAll('a.btn')
        Array.from(buttons).map( button => button.addEventListener('click', evt => {
                evt.preventDefault()
                const
                    clickedRouteName = evt.target.href.split('/').pop() || 'about'
                  , oldRouteName = Router.getRequestName()
                if (oldRouteName !== clickedRouteName) {
                    this.onRouteChange(clickedRouteName)
                    history.pushState(null, null, evt.target.href)
                    evt.stopPropagation()
                }
            }, false)
        )

        window.addEventListener('popstate', evt => this.onRouteChange(), false)

        this.onRouteChange()

    }

    onRouteChange (reqName = Router.getRequestName()) {
        const
            routeNames = ['notfound','about','preview','timeline','edit','advanced']
          , $html = document.querySelector('html')
        if ( -1 === routeNames.indexOf(reqName) ) reqName = 'notfound'
        $html.classList.remove.apply($html.classList
          , routeNames.map( n => 'route-'+n ) )
        $html.classList.add('route-'+reqName)
        this.onWindowResize()
    }

    static getRequestName (query=location.search) {
        return '' === query ? 'about'
          : '?/' === query ? 'about'
          : '?/' === query.slice(0,2) ? query.slice(2)
          : 'notfound'
    }

}
