# Undo3D Animated Logo

#### The Undo3D low-poly logo, with some simple lighting transitions

[Home](https://undo3d.gitlab.io/animated-logo)
[Repo](https://gitlab.com/Undo3D/animated-logo)

The Undo3D logo has [these settings:
](https://undo3d.gitlab.io/animated-logo/#paused,3000,10000,212,45,hide,0,30,hide)

- Timestamp 3000 of 10000
- Single-light 212° 45%
- Pair-light 0° 30%

Texture metalness is 0.1, ambient light is 100% white, 0.4 intensity.  
Light positions and rotations are defined in main/scene.mjs:  
```js
//// Setting up the scene...
this.lightBoomSingle.position.set(256, -256, 100)
this.pointLightSingle.position.set(50, -240, -100)
this.lightBoomPair.position.set(220, -265, 110)
this.pointLightPair1.position.set(220, 40, 0)
this.pointLightPair2.position.set(220, -40, 0)

//// ...and during render.
this.lightBoomSingle.rotation.z = Math.PI * -2 - rads
this.lightBoomSingle.rotation.y = Math.PI - rads
this.lightBoomPair.rotation.z = Math.PI + rads
```
